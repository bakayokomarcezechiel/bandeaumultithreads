package bandeau;

public class ExerciceAvecThreads {

    public static void main(String[] args) {
        ExerciceAvecThreads instance = new ExerciceAvecThreads();
        instance.exemple();
    }

    public void exemple() {
        
        Scenario s = makeScenario();
        // On cree les bandeaux
        Bandeau b1 = new Bandeau();
        Bandeau b2 = new Bandeau();
        Bandeau b3 = new Bandeau();
        System.out.println("CTRL-C pour terminer le programme");

        // On doit jouer le scénario en même temps sur les trois bandeaux
        // s.playOn(b1);
        // s.playOn(b2);
        // s.playOn(b3);
        // Attendez que le scénario se termine
  
        // On rejoue le scénario sur b1 quand le premier jeu est fini
        // s.playOn(b1);

        System.out.println("CTRL-C pour terminer le programme");

        // Je creer plusieurs threads pour exécuter le même scénario sur différents bandeaux
        Thread thread1 = new Thread(() -> {
            s.assignBandeau(b1);
            s.playOn(b1);
        });

        Thread thread2 = new Thread(() -> {
            s.assignBandeau(b2);
            s.playOn(b2);
        });

        Thread thread3 = new Thread(() -> {
            s.assignBandeau(b3);
            s.playOn(b3);
        });

        // Je demarre les threads
        thread1.start();
        thread2.start();
        thread3.start();

        // J'attend que tous les threads se terminent
        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Je Rejoue le scénario sur b1 après que le premier jeu est fini
        s.assignBandeau(b1);
        s.playOn(b1);

    }

    private Scenario makeScenario() {
        // On crée un scenario
        Scenario s = new Scenario();
        // On lui ajoute des effets
        s.addEffect(new RandomEffect("Le jeu du pendu", 700), 1);
        // s.addEffect(new TeleType("Je m'affiche caractère par caractère", 100), 1);
        // s.addEffect(new Blink("Je clignote 10x", 100), 10);
        // s.addEffect(new Zoom("Je zoome", 50), 1);
        // s.addEffect(new FontEnumerator(10), 1);
        s.addEffect(new Rainbow("Comme c'est joli !", 30), 1);
        //s.addEffect(new Rotate("2 tours à droite", 180, 4000, true), 2);
        //s.addEffect(new Rotate("2 tours à gauche", 180, 4000, false), 2);
        return s;
    }
}
